# Calibre recipe for Instapaper.com (Development version)
# 
# Homepage: http://khromov.wordpress.com/projects/instapaper-calibre-recipe/
# Code Repository: https://bitbucket.org/khromov/calibre-instapaper

import urllib, sys
from calibre import replace_entities
from calibre.web.feeds.news import BasicNewsRecipe

class AdvancedUserRecipe1299694372(BasicNewsRecipe):
    title                         = u'Instapaper (devel)'
    __author__                    = 'Darko Miletic, Stanislav Khromov, Jim Ramsay'
    publisher                     = 'Instapaper.com'
    category                      = 'info, custom, Instapaper'
    feeds = []

    #if all feeds are empty, no periodical will be generated
    remove_empty_feeds = True

    # - Recipe configuration - #

    #Set archive to True if you want to auto-archive items
    #Note: does not archive "liked" items, only 'Unread' items (and folder items)
    archive    = False

    #Set include_liked to True if you want to include liked items in your feed.
    include_liked    = False

    #Set to True if you want to reverse article order.
    reverse_article_order = False

    #Set the maximum amount of articles you'd like to receive here
    max_articles_per_feed = 30

    #Adds the title tag to the body of the recipe. Use this if your articles miss headings.
    add_title_tag = False;

    #Set your own custom folders here, simply re-paste the example line below for each new folder you need
    #Example:
    #feeds+=[(u'My custom folder', u'http://www.instapaper.com/u/folder/1321400/my-custom-folder')]
    #feeds+=[(u'Long-term reading', u'http://www.instapaper.com/u/folder/1232089/long-term-reading')]

    # - End of recipe configuration - #

    oldest_article        = 0
    no_stylesheets        = True
    remove_javascript     = True
    remove_tags              = [
        dict(name='div', attrs={'id':'text_controls_toggle'})
        ,dict(name='script')
        ,dict(name='div', attrs={'id':'text_controls'})
        ,dict(name='div', attrs={'id':'editing_controls'})
        ,dict(name='div', attrs={'class':'bar bottom'})
        ,dict(name='div', attrs={'id':'controlbar_container'})
        ,dict(name='div', attrs={'id':'footer'})
         ]
    use_embedded_content  = False
    needs_subscription    = True
    INDEX                 = u'http://www.instapaper.com'
    LOGIN                 = INDEX + u'/user/login'
    
    if include_liked:
        feeds = feeds+[(u'Instapaper Liked', u'http://www.instapaper.com/starred')]

    feeds = feeds+[(u'Instapaper Unread', u'http://www.instapaper.com/u')]
    feeds.reverse()
    
    cleanup_items = []
    cleanup_uris = {}

    def get_browser(self):
        br = BasicNewsRecipe.get_browser()
        if self.username is not None:
            br.open(self.LOGIN)
            br.select_form(nr=0)
            br['username'] = self.username
            if self.password is not None:
                br['password'] = self.password
            br.submit()
        return br

    def parse_instapaper_date(self, date):
        if date == u'today':
            t = time.localtime()
            return u'%d/%d' % (t.tm_mon, t.tm_mday)
        return date

    def parse_index(self):
        totalfeeds = []
        lfeeds = self.get_feeds()

        for feedobj in lfeeds:
            feedtitle, feedurl = feedobj
            articles = []

            #folder page
            current_page = 1
            
            #so we run the first time
            articles_on_page = 1

            #download articles while they are available and we don't have enough to satisfy max_articles_per_feed
            while (articles_on_page>0 and len(articles)<self.max_articles_per_feed):
                self.report_progress(0, _('Fetching feed')+' %s (%d)...'%((feedtitle if feedtitle else feedurl), current_page))
                soup = self.index_to_soup(feedurl+u'/'+str(current_page))

                #get and count number of items on current page
                items = soup.findAll('a', attrs={'class':'actionButton textButton'})
                articles_on_page = len(items)

                # Go through each item, (each item is the 'Text' link) looking
                # for the archive link, and secondary information (for
                # description and date)
                for item in items:
                    if item.has_key('href'):
                        article = {'url':item['href']}

                        # The 'Archive' button is in the same parent as 'Text'
                        archive = item.parent.find('a', attrs={'class':'actionButton archiveButton'})
                        if archive and archive.has_key('href'):
                            self.cleanup_uris[item['href']] = archive;

                        # The 'secondaryControls' div (in the grandparent of the
                        # 'Text' button) contains a bunch of class="host" spans:
                        # source site, date added, Share, Edit, [Move,] Delete
                        secondary = item.parent.parent.find('div', attrs={'class':'secondaryControls'})
                        if secondary:
                            spans = secondary('span', attrs={'class':'host'})
                            if len(spans) >= 2:
                                source = spans[0].string.strip()
                                article['description'] = source
                                article['date'] = self.parse_instapaper_date(spans[1].string.strip())

                        # The 'summary' div in the grandparent of the 'Text' button
                        # contains a summary we should use if available.
                        summary = item.parent.parent.find('div', attrs={'class':'summary'})
                        if summary and len(summary.string.strip()) > 0:
                            article['description'] = summary.string.strip()
                            if source:
                                article['description'] += u' [%s]' % source

                        #print "Adding article:", article
                        articles.append(article)
                current_page+=1
                
            #Don't append empty feeds
            if len(articles)!=0:
                totalfeeds.append((feedtitle, articles))
        
        if len(totalfeeds)==0:
            return None
        else:
            return totalfeeds

    def print_version(self, url):
        return 'http://www.instapaper.com' + url

    def populate_article_metadata(self, article, soup, first):
        #adds the title to the metadata
        article.title = replace_entities(soup.find('title').contents[0].strip())
        #Now that we know this article is downloaded, set it to be cleaned up
        self.cleanup_items.append(self.cleanup_uris[article.url])

    def postprocess_html(self, soup, first_fetch):
        #adds the title to each story, as it is not always included
        if self.add_title_tag:
            for link_tag in soup.findAll(attrs={"id" : "story"}):
                link_tag.insert(0,'<h1>'+soup.find('title').contents[0].strip()+'</h1>')

        #print repr(soup)
        return soup

    def cleanup(self):
        if self.archive:
            self.report_progress(0, "Archiving articles...")
            # Clean up each item
            for cleanup_item in self.cleanup_items:
                try:
                    self.report_progress(0, "Cleaning up... : " + self.INDEX+cleanup_item['href'])
                    self.browser.open(self.INDEX+cleanup_item['href']);
                except Exception, e:
                    #print "Exception caught:", e
                    self.report_progress(0, "Problem detected, skipping cleanup for this article")
                    pass
